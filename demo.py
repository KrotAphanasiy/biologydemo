import cv2 as cv
from keras_retinanet.utils.image import preprocess_image
from keras_retinanet.utils.image import read_image_bgr
from keras_retinanet.utils.image import resize_image
from config import  config
from keras_retinanet import models
from imutils import paths
import numpy as np
import argparse

def EmphObject(img, box, color):
    (startX, startY, endX, endY) = box

    img = cv.rectangle(img, (startX, startY), (endX, endY), (0, 255, 0), 2)

    return img

ap = argparse.ArgumentParser()
ap.add_argument("--model", required=True)
ap.add_argument("--video", required=True)
ap.add_argument("--out", required=False)
args = vars(ap.parse_args())


model = models.load_model(args["model"])

cap = cv.VideoCapture(args["video"])
fourcc = cv.VideoWriter_fourcc(*'XVID')
write = cv.VideoWriter(args["out"], fourcc, 20.0, (1920, 1080))

while True:
    ret, frame = cap.read()
    cv.imshow("frame", frame)
    frame = cv.resize(frame, (1920, 1080))

    image = preprocess_image(frame)
    cv.imshow("prerocessed", image)
    ready = np.expand_dims(image, axis=0)

    (boxes, scores, labels) = model.predict_on_batch(ready)

    flyCount = 0
    rainCount = 0

    for (box, score, label) in zip(boxes[0], scores[0], labels[0]):
        if score > 0.75:
            box = box.astype("int")
            (startX, startY, endX, endY) = box
            result = EmphObject(frame, box, (0,255,0))

            labelStr = str()

            if label == 0:
                labelStr = "Maggot"
                flyCount += 1
            elif label == 1:
                labelStr = "Earthworm"
                rainCount += 1

            result = cv.putText(result, labelStr, (startX-25, startY - 25), cv.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255),
                                         thickness=2)


    result = cv.putText(result, "Maggots: " + str(flyCount), (1400, 880), cv.FONT_HERSHEY_SIMPLEX, 2, (0,255,0),
                        thickness=3)

    result = cv.putText(result, "Earthworms: " + str(rainCount), (1400, 980), cv.FONT_HERSHEY_SIMPLEX, 2, (0,255,0),
                        thickness=3)

    cv.imshow("res", result)
    write.write(result)
    flyCount = 0
    rainCount = 0

    if cv.waitKey(1) & 0xFF == ord('q'):
        break

cap.release()
write.release()
cv.destroyAllWindows()


