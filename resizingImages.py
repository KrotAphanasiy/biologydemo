import cv2 as cv
import argparse
import imutils
from imutils import paths

ap = argparse.ArgumentParser()
ap.add_argument("-p", "--path", required=True)
args = vars(ap.parse_args())

images_path = args["path"]

imagePaths = list(paths.list_files(images_path))

for path in imagePaths:
    print(path)
    image = cv.imread(path)
    cv.imshow("image", image)
    resized = cv.resize(image , (200, 200))
    cv.imshow("resized", resized)
    #cv.waitKey()
    cv.imwrite(path, resized)
    
    