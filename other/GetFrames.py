import argparse
import cv2 as cv
import random
import imutils

argumentParser = argparse.ArgumentParser()
argumentParser.add_argument("-v", "--video", required=True)
argumentParser.add_argument("-p", "--path", required=True)
argumentParser.add_argument("-s",  "--skip", required=True)
args = vars(argumentParser.parse_args())


cap = cv.VideoCapture(args["video"])

skipFrameRate = int(args["skip"])

pathToSave = args["path"]

frameCounter = 0
savedCounter = 675

while cap.isOpened():
    ret, frame = cap.read()
    frame = cv.resize(frame, (640, 480))
    hsvFrame = cv.cvtColor(frame, cv.COLOR_BGR2HSV)
    grayFrame = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)
    cv.imshow("orig", frame)
    cv.imshow("hsv", hsvFrame)
    cv.imshow("gray", grayFrame)

    angle = random.randrange(0, 360, 15)
    rotatedFrame = imutils.rotate(frame, angle)
    cv.imshow("rotated", rotatedFrame)

    if frameCounter % skipFrameRate == 0:
        cv.imwrite(pathToSave + "/rgb/" + str(savedCounter) + ".jpg", rotatedFrame)
        savedCounter += 1

    frameCounter += 1

    if cv.waitKey(1) & 0xFF == ord('q'):
        break

cap.release()
cv.destroyAllWindows()